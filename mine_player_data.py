"""
Loads information from a <player>.dat file and inserts it into an SQLite
database.
"""

import os.path
import re
import traceback

from quarry.types.nbt import *

from utils import *


def process_player(c, player, noext = None, players_are_entities = False):
    en = player.value
    x, y, z = map(lambda coord: coord.value, en["Pos"].value)
    uuid = get_uuid(en)

    if noext is not None and noext != uuid:
        print("WARNING: UUID from filename ({}) does not match UUID from NBT ({}). Using the UUID from NBT canonically.".format(noext, uuid))

    c.execute('INSERT OR REPLACE INTO players (uuid, file_uuid) VALUES (?, ?)', (uuid, noext))

    if players_are_entities:
        nbt = player.to_bytes()
        c.execute('INSERT OR REPLACE INTO entities (x, y, z, mcid, uuid, nbt) VALUES (?, ?, ?, ":player", ?, ?)', (x, y, z, uuid, nbt))

    if "Inventory" in en:
        inv = en["Inventory"].to_bytes()
        c.execute('INSERT OR REPLACE INTO player_inventories (player_uuid, inventory_type, nbt) VALUES (?, ?, ?)', (uuid, INVENTORY_TYPE_MAIN, inv))

    if "EnderItems" in en:
        ec = en["EnderItems"].to_bytes()
        c.execute('INSERT OR REPLACE INTO player_inventories (player_uuid, inventory_type, nbt) VALUES (?, ?, ?)', (uuid, INVENTORY_TYPE_ENDER_CHEST, ec))

def main(argv):
    # Parse options
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--database", default="world.db", help="database file to write mined data to")
    parser.add_argument("--players-are-entities", action='store_true', help="if set, players will be added to the entities list")
    parser.add_argument("path", nargs = "+", help="paths of player data files to load. If a single directory is specified, all .dat files in it will be considered")
    args = parser.parse_args(argv)

    db = MCWMDB(args.database)

    if len(args.path) == 1 and os.path.isdir(args.path[0]):
        player_files = [os.path.join(args.path[0], filename) for filename in os.listdir(args.path[0]) if filename.endswith('.dat')]
    else:
        player_files = args.path

    for path in player_files:
        try:
            file = NBTFile.load(path)
            player = file.root_tag.body
            basename = os.path.basename(path)
            noext = os.path.splitext(basename)[0]

            process_player(db.cursor(), player, noext, args.players_are_entities)
        except Exception:
            traceback.print_exc()
            print("For above exception: occurred while processing file {}".format(path))
    db.commit()

if __name__ == "__main__":
    import sys
    main(sys.argv[1:])
