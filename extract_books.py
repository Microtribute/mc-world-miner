"""
Extracts book texts from the items extracted by process_inventories.py.
"""

import os.path
import re

from quarry.types.nbt import *

from utils import *

def main(argv):
    # Parse options
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--database", default="world.db", help="database file to write mined data to")
    parser.add_argument("--no-written-books", action='store_true', help="if set, written books will not be processed")
    parser.add_argument("--no-books-and-quills", action='store_true', help="if set, books and quills will not be processed")
    args = parser.parse_args(argv)

    db = MCWMDB(args.database)

    c = db.cursor()
    desired_mcids = (["minecraft:written_book"] if not args.no_written_books else []) + (["minecraft:writable_book"] if not args.no_books_and_quills else [])
    query = "SELECT id, nbt FROM items WHERE mcid IN ({})".format(','.join('?' * len(desired_mcids)))
    result = c.execute(query, desired_mcids).fetchall()

    for id, nbt in result:
        book = TagCompound.from_bytes(nbt).value
        if 'pages' in book:
            pages = [page.value for page in book['pages'].value]
        else:
            pages = []

        title = book['title'].value if 'title' in book else None
        author = book['author'].value if 'author' in book else None
        generation = book['generation'].value if 'generation' in book else 0
        display_name = book['display'].value['Name'].value if 'display' in book and 'Name' in book['display'].value else None

        content_hash = hash((title, author) + tuple(pages))
        c.execute("INSERT OR IGNORE INTO books (item_id, content_hash, generation, display_name) VALUES (?, ?, ?, ?)", (id, content_hash, generation, display_name))
        c.execute("INSERT OR IGNORE INTO book_metadata (content_hash, title, author) VALUES (?, ?, ?)", (content_hash, title, author))
        for page_number, page in enumerate(pages):
            c.execute("INSERT OR IGNORE INTO book_pages (content_hash, page_number, content) VALUES (?, ?, ?)", (content_hash, page_number, page))

    db.commit()

if __name__ == "__main__":
    import sys
    main(sys.argv[1:])
